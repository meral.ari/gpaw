.. _literature:

----------
Literature
----------


Links to guides and manual pages
--------------------------------

* The GPAW calculator :ref:`manual`

* The :ref:`devel` pages

* The :ref:`guide for developers <developersguide>`

* The code :ref:`overview`

* The :ref:`features and algorithms` in GPAW


.. _literature_reports_presentations_and_theses:

Reports, presentations, and theses using gpaw
---------------------------------------------

* Summer-school 2014 talk about PAW, GPAW and ASE: :download:`ss14.pdf`

* A short note on the basics of PAW: :download:`paw_note.pdf`

* A master thesis on the inclusion of non-local exact exchange in the
  PAW formalism, and the implementation in gpaw:
  :download:`rostgaard_master.pdf`

* A master thesis on the inclusion of a localized basis in the PAW
  formalism, plus implementation and test results in GPAW:
  :download:`marco_master.pdf`

* A master thesis on the inclusion of localized basis sets in the PAW
  formalism, focusing on basis set generation and force calculations:
  :download:`askhl_master.pdf`

* A course report on a project involving the optimization of the
  setups (equivalent of pseudopotentials) in gpaw:
  :download:`askhl_10302_report.pdf`

* Slides from a talk about PAW: :download:`mortensen_paw.pdf`

* Slides from a talk about GPAW development:
  :download:`mortensen_gpaw-dev.pdf`

* Slides from a mini symposium during early development stage:
  :download:`mortensen_mini2003talk.pdf`


.. _paw_papers:

Articles on the PAW formalism
-----------------------------

The original article introducing the PAW formalism:
   | P. E. Blöchl
   | :doi:`Projector augmented-wave method <10.1103/PhysRevB.50.17953>`
   | Physical Review B, Vol. **50**, 17953, 1994

A different formulation of PAW by Kresse and Joubert designed to make the transition from USPP to PAW easy.
  | G. Kresse and D. Joubert
  | :doi:`From ultrasoft pseudopotentials to the projector augmented-wave method <10.1103/PhysRevB.59.1758>`
  | Physical Review B, Vol. **59**, 1758, 1999

A second, more pedagogical, article on PAW by Blöchl and co-workers.
  | P. E. Blöchl, C. J. Först, and J. Schimpl
  | :doi:`Projector Augmented Wave Method: ab-initio molecular dynamics with full wave functions <10.1007/BF02712785>`
  | Bulletin of Materials Science, Vol. **26**, 33, 2003


.. _gpaw_publications:

Citations of the GPAW method papers
-----------------------------------

.. image:: citations.png
   :width: 750

(updated on 18 Mar 2021)

The total number of citations above is the number of publications
citing at least one of the other papers, not the sum of all citation
counts.

See :ref:`citation` for a list of the method papers.

All citing articles:
